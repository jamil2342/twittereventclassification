from caching import Cache

from DBPediaSPARQL import ExtractHirerchy
from DbpediaResponse import dbpedia_response_from_dict
from BaseNE import BaseNE
import json
import requests
class DBPediaNE(BaseNE):
    mostSpecific : bool
    def __init__(self, mostSpecific=True):
        self.mostSpecific = mostSpecific
        return

    @Cache(ttl=-1, maxsize=50000, filepath='DbPediaSpotlightCacheFile')
    def Extract1(self, str,mostSpecific):
        # doc Washington was the first president of USA
        # I am going to washington
        # What is the related resource of our sentence. From the sentence we have to know this
        # is which Washington. President Washington or copital Washington.
        url = "https://api.dbpedia-spotlight.org/en/annotate"
        #querystring = {"text": urllib.parse.quote(str)}
        querystring = {"text": str}
        headers = {
            'accept': "application/json",
            'User-Agent': "PostmanRuntime/7.15.2",
            'Cache-Control': "no-cache",
            'Postman-Token': "8c039165-430d-4194-aeb1-701b99702f28,657776dc-5b50-4c13-b826-de7d6d6a6eba",
            'Host': "api.dbpedia-spotlight.org",
            'Accept-Encoding': "gzip, deflate",
            'Connection': "keep-alive",
            'cache-control': "no-cache"
        }

        response = requests.request("GET", url, headers=headers, params=querystring)
        try:
            result = dbpedia_response_from_dict(json.loads(response.text))
        except:
            print(response.text)
            return []
        arr = []
        for res in result.resources:
            strUri = '<' + res.uri+'>'
            lst = ExtractHirerchy(strUri)
            if len(lst)==0:
                continue
            elif len(lst)==1:
                arr.append((res.surface_form, lst[0]))
                continue
            if mostSpecific:
                arr.append((res.surface_form, lst[-1]))
            else:
                arr.append((res.surface_form, lst[0]))
        return arr;

    def Extract(self,str):
        return self.Extract1(str, self.mostSpecific)



