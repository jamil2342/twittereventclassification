# To use this code, make sure you
#
#     import json
#
# and then, to convert JSON from a string, do
#
#     result = dbpedia_response_from_dict(json.loads(json_string))

from typing import Any, List, TypeVar, Callable, Type, cast
import requests


T = TypeVar("T")


def from_str(x: Any) -> str:
    assert isinstance(x, str)
    return x


def from_list(f: Callable[[Any], T], x: Any) -> List[T]:
    assert isinstance(x, list)
    return [f(y) for y in x]


def to_class(c: Type[T], x: Any) -> dict:
    assert isinstance(x, c)
    return cast(Any, x).to_dict()


class Resource:
    uri: str
    support: int
    types: str
    surface_form: str
    offset: int
    similarity_score: str
    percentage_of_second_rank: str

    def __init__(self, uri: str, support: int, types: str, surface_form: str, offset: int, similarity_score: str, percentage_of_second_rank: str) -> None:
        self.uri = uri
        self.support = support
        self.types = types
        self.surface_form = surface_form
        self.offset = offset
        self.similarity_score = similarity_score
        self.percentage_of_second_rank = percentage_of_second_rank

    @staticmethod
    def from_dict(obj: Any) -> 'Resource':
        assert isinstance(obj, dict)
        uri = from_str(obj.get("@URI"))
        support = int(from_str(obj.get("@support")))
        types = from_str(obj.get("@types"))
        surface_form = from_str(obj.get("@surfaceForm"))
        offset = int(from_str(obj.get("@offset")))
        similarity_score = from_str(obj.get("@similarityScore"))
        percentage_of_second_rank = from_str(obj.get("@percentageOfSecondRank"))
        return Resource(uri, support, types, surface_form, offset, similarity_score, percentage_of_second_rank)

    def to_dict(self) -> dict:
        result: dict = {}
        result["@URI"] = from_str(self.uri)
        result["@support"] = from_str(str(self.support))
        result["@types"] = from_str(self.types)
        result["@surfaceForm"] = from_str(self.surface_form)
        result["@offset"] = from_str(str(self.offset))
        result["@similarityScore"] = from_str(self.similarity_score)
        result["@percentageOfSecondRank"] = from_str(self.percentage_of_second_rank)
        return result


class DbpediaResponse:
    text: str
    confidence: str
    support: int
    types: str
    sparql: str
    policy: str
    resources: List[Resource]

    def __init__(self, text: str, confidence: str, support: int, types: str, sparql: str, policy: str, resources: List[Resource]) -> None:
        self.text = text
        self.confidence = confidence
        self.support = support
        self.types = types
        self.sparql = sparql
        self.policy = policy
        self.resources = resources

    @staticmethod
    def from_dict(obj: Any) -> 'DbpediaResponse':
        assert isinstance(obj, dict)
        text = from_str(obj.get("@text"))
        confidence = from_str(obj.get("@confidence"))
        support = int(from_str(obj.get("@support")))
        types = from_str(obj.get("@types"))
        sparql = from_str(obj.get("@sparql"))
        policy = from_str(obj.get("@policy"))
        resources = from_list(Resource.from_dict, obj.get("Resources"))
        return DbpediaResponse(text, confidence, support, types, sparql, policy, resources)

    def to_dict(self) -> dict:
        result: dict = {}
        result["@text"] = from_str(self.text)
        result["@confidence"] = from_str(self.confidence)
        result["@support"] = from_str(str(self.support))
        result["@types"] = from_str(self.types)
        result["@sparql"] = from_str(self.sparql)
        result["@policy"] = from_str(self.policy)
        result["Resources"] = from_list(lambda x: to_class(Resource, x), self.resources)
        return result


def dbpedia_response_from_dict(s: Any) -> DbpediaResponse:
    return DbpediaResponse.from_dict(s)


def dbpedia_response_to_dict(x: DbpediaResponse) -> Any:
    return to_class(DbpediaResponse, x)

