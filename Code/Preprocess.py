import re
import string



def Preprocess(str):
    str = str.lower()
    # Replace Url
    # copyright https://stackoverflow.com/questions/11331982/how-to-remove-any-url-within-a-string-in-python
    # matches any non-whitespace character
    str = re.sub('http[s]?://\S+', 'URL', str)

    #Replace email
    # copyright https://stackoverflow.com/questions/17681670/extract-email-sub-strings-from-large-document
    # \w  matches any alphanumeric character
    str = re.sub(r'[\w\.-]+@[\w\.-]+', 'Email', str, flags=re.MULTILINE)

    #Remove Emojify
    #copyright https://stackoverflow.com/questions/33404752/removing-emojis-from-a-string-in-python
    str.encode('ascii', 'ignore').decode('ascii')
    return str;
# print(Preprocess('@peter I really love that shirt at #Macy. http://bet.ly//WjdiW4'))

