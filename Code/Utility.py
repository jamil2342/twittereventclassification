class Utility:
    @staticmethod
    def countInAList(inputList=[], input=""):
        count = 0
        for iter in inputList:
            if iter == input:
                count = count + 1
        return count


    # ## compare two 2d array. Return true if all the element match otherwise return false. Implemented for debugging only.

    @staticmethod
    def compareArray(first_array=[[]], second_array=[[]]):
        for i in range(len(first_array)):
            for j in range(len(first_array[0])):
                if first_array[i][j] != second_array[i][j]:
                    return False
        return True


    # ## The function behaves similar of multilavelbinarizer transform method. As frequency is require for some classifier like knn, so we give option to count frequency in the return array baseon isBinaryMap variable

    # In[12]:

    @staticmethod
    def GetMapping(arr, _classes=[], isBinaryMap=True):
        shp = (len(arr), len(_classes))
        outputArray = np.zeros(shp)
        if isBinaryMap:
            for i in range(len(arr)):
                for j in range(len(_classes)):
                    outputArray[i][j] = int(_classes[j] in arr[i])

        else:
            for i in range(len(arr)):
                for j in range(len(_classes)):
                    outputArray[i][j] = arr[i].count(_classes[j])  # countInAList(train_tokens[i],common_word[j])

        return outputArray


    # ## This function removes the word which contains symbol only like "% from the most common word. As this sort of word don't create any value for categorization.

    # In[24]:

    @staticmethod
    def ContainsAlphabet(s):
        ASCII_LOWERCASE = "abcdefghijklmnopqrstuvwxyz"
        ASCII_UPPERCASE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        ALPHABETS = ASCII_LOWERCASE + ASCII_UPPERCASE
        # c= str(s)[0]
        for c in str(s):
            if c in ALPHABETS:
                return True

        return False
