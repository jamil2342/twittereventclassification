import spacy
spacy.load('en_core_web_sm')
lemmatizer = spacy.lang.en.English()

def my_lemmatizer(my_str):
    tokens = lemmatizer(my_str)
    return([token.lemma_ for token in tokens])

