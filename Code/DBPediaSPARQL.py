from functools import lru_cache
from caching import Cache
from SPARQLWrapper import SPARQLWrapper, JSON


# resourceDic = {}

# @lru_cache(maxsize=5000)

# doc here ttl means time to live
@Cache(ttl=-1, maxsize=50000, filepath='DbPediaHirerchyCacheFile')
def ExtractHirerchy(resource):
    # if resource in resourceDic.keys():
    #     return resourceDic[resource]
    #copyright https://stackoverflow.com/questions/43761220/finding-super-classes-of-an-entity-in-sparql/43769030#43769030
    #https://rdflib.github.io/sparqlwrapper/
    sparql = SPARQLWrapper("http://dbpedia.org/sparql")
    qry = """PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
                SELECT ?sc1 ?sc2
                 WHERE
                 {
                  #res a ?sc1;
                                                                   a ?sc2.
                  ?sc1 rdfs:subClassOf ?sc2.
                  FILTER (strstarts(str(?sc1), "http://dbpedia.org/ontology/"))
                 }"""
    qry = qry.replace('#res',resource)
    sparql.setQuery(qry)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()
    arr = []
    Dict = {}
    for result in results["results"]["bindings"]:
        sc1 = str(result["sc1"]["value"]).replace('http://dbpedia.org/ontology/','')
        sc2 = str(result["sc2"]["value"]).replace('http://dbpedia.org/ontology/', '')
        Dict[sc2] = sc1
        # arr.append(sc1)

    # del arr[1::2]
    iter = 'http://www.w3.org/2002/07/owl#Thing'
    while True:
        if iter not in Dict:
            break
        iter = Dict[iter]
        arr.append(iter)
    # if len(arr)>0:
    #     resourceDic[resource] = arr
    return arr