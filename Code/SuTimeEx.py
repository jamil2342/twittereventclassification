import json
import os
from sutime import SUTime

from BaseNE import BaseNE


class SuTimeEx (BaseNE):

    jar_files = os.path.join(os.path.dirname(__file__), 'jars')
    sutime = SUTime(jars=jar_files, mark_time_ranges=True)

    def __init__(self):
        return


    def Extract(self, test_case):
        strArray = self.sutime.parse(test_case)
        lst = []
        for str in strArray:
            lst.append((str['text'], str['type']))
        return lst

# test_case = u'I need a desk for tomorrow from 2pm to 3pm'
# extr = SuTimeEx()
#
# str = extr.Extract(test_case)
# print (str)
