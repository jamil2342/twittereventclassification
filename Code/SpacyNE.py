import spacy
from spacy import displacy

from BaseNE import BaseNE

nlp = spacy.load("en_core_web_sm")

class SpacyNE(BaseNE):
    def __init__(self):
        return
    def Extract(self, str):
        arr = []
        str = nlp(str)
        for X in str.ents:
            arr.append((X.text, X.label_))
        return arr;

