from nltk.corpus import stopwords
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score
from sklearn.naive_bayes import BernoulliNB, MultinomialNB
from sklearn.svm import SVC
import numpy as np
from sklearn import metrics
from sklearn.naive_bayes import BernoulliNB
from sklearn import svm
from sklearn.neighbors import KNeighborsClassifier, NearestCentroid
from pandas import DataFrame
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split, KFold
import Utility
from DBPediaNE import DBPediaNE
from Lemmatizer import my_lemmatizer
from NamedEntityReplacement import NamedEntityReplacement
from Preprocess import Preprocess
from SpacyNE import SpacyNE
from StanfordNE import StanfordNE
from SuTimeEx import SuTimeEx

df = DataFrame.from_csv("all_descriptions_categories.tsv", sep="\t")
dataset = df.values

# doc first we have to shuffle. So, that homoginity might be reduced.
np.random.shuffle(dataset)
X = np.empty(len(dataset), dtype=object)
y = np.empty(len(dataset), dtype=object)

for row in enumerate(dataset):
    doc = row[1][0]
    r = row[0]
    cat = row[1][1]
    if isinstance(cat, str) == False:
        continue
    X[r] = doc  # formless.GetNEReplacement(doc)
    y[r] = cat

cat_list = set(y)
for iter in cat_list:
    print(iter)

def SingleStepClassification():
    # copyright https://towardsdatascience.com/hacking-scikit-learns-vectorizers-9ef26a7170af
    stopWords = stopwords.words('english')
    # doc binary = false means high variance and true means high bias
    vectorizer = CountVectorizer(binary = False, lowercase=True, max_features=500, stop_words=stopWords, tokenizer=my_lemmatizer)

    # doc describe the pros and cons of each classifier
    clfs = []# [BernoulliNB()]
    clfs.append(NearestCentroid())
    clfs.append(BernoulliNB())
    clfs.append(KNeighborsClassifier(n_neighbors=5))
    #clfs.append(svm.SVC())


    # doc here the order is important. We have to use the ne which can find more specific first
    # for exqmple if we tell donald trump is coming to Germany
    # dbpediane can find President is coming to Country. which means this is something about politics.
    # where as stanford ne is finding Person is coming to Place
    # so, we have to use the ne which is creating more value for us.
    # on that list we are taking the baseclass object and catching the derived class constructors.
    # getting the flavour of the polymorphysm
    neLst = []
    neLst.append(DBPediaNE(mostSpecific=True))

    # doc Here we are modifying the te[mporal information
    # I need a desk for tomorrow from 2pm to 3pm
    # I need a desk for [Date] from [Duration]
    # copyright example taken from https://github.com/FraBle/python-sutime
    neLst.append(SuTimeEx())
    #neLst.append(SpacyNE())
    neLst.append(StanfordNE())
    nameEntityReplacer = NamedEntityReplacement(neList= neLst)

    for iclf, clf in enumerate(clfs):
        cross_validation = KFold(n_splits=5)
        acc_list = []
        prec_list = []
        rec_list = []
        f_list = []
        # copyright https://github.com/nawabhussain/sentiment_analysis
        for ifold, (train_index, test_index) in enumerate(cross_validation.split(X)):
            X_train, X_test = X[train_index], X[test_index]
            y_train, y_test = y[train_index], y[test_index]

            for iter in range(len(X_train)):

                # doc trump is coming to Germany
                # putin is going to USA
                # President is going to Country
                X_train[iter] = nameEntityReplacer.GetNEReplacement(X_train[iter])

                # doc Replacing url and email with tag
                # Please search to http://www.google.com
                # Please search to [URL]12
                # Please write an email to jamil.0505038@gmail.com
                # Please write an email to [Email]
                X_train[iter] = Preprocess(X_train[iter])

            for iter in range(len(X_test)):
                X_test[iter] = nameEntityReplacer.GetNEReplacement(X_test[iter])
                X_test[iter] = Preprocess(X_test[iter])


            train_vectors = vectorizer.fit_transform(X_train)
            test_vectors = vectorizer.transform(X_test)
            print(vectorizer.get_feature_names())
            clf.fit(train_vectors, y_train)
            prediction = clf.predict(test_vectors)
            accur = accuracy_score(y_test, prediction)
            prec = precision_score(y_test, prediction, average='macro')
            recall = recall_score(y_test, prediction, average='macro')
            f1Score = f1_score(y_test, prediction, average='macro')
            acc_list.append(accur)
            prec_list.append(prec)
            rec_list.append(recall)
            f_list.append(f1Score)

        avgAccuracy = np.mean(acc_list)
        avgPrecision = np.mean(prec_list)
        avgRecall = np.mean(rec_list)
        avgFscore = np.mean(f_list)
        print("\n Accuracy ", avgAccuracy)
        print("\n Precission ", avgPrecision)
        print("\n Recall ", avgRecall)
        print("\n F1 ", avgFscore)

SingleStepClassification()

