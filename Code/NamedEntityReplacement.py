from BaseNE import BaseNE
from DBPediaNE import  DBPediaNE
from SpacyNE import SpacyNE
from typing import Any, List, TypeVar, Callable, Type, cast

class NamedEntityReplacement(object):
    neList = []
    def __init__(self,neList):
        self.neList = neList;
        return
    def GetNEReplacement(self, inputStr ):
        output = str(inputStr);
        for DBNE in self.neList:
            arr = DBNE.Extract(inputStr);
            for element in arr:
                output = output.replace(element[0], element[1])
        return output;
