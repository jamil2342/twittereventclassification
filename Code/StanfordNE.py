import os

import nltk
from nltk.tag import StanfordNERTagger

from BaseNE import BaseNE


class StanfordNE(BaseNE):
    st = StanfordNERTagger('classifiers/english.all.3class.distsim.crf.ser.gz',
                           'jars/stanford-ner.jar')

    def __init__(self):
        return

    def Extract(self, str):
        arr = self.st.tag(str.split())
        outArr = []
        for iter in arr:
            if iter[1] != 'O':
                outArr.append(iter)
        return outArr

# stne = StanfordNE()
# out = stne.Extract('Rami Eid is studying at Stony Brook University in NY')
# print(out)
