# Events 2012 Twitter Dataset

You will need to use a tool such as Twitter-dataset-collector 
(https://github.com/socialsensor/twitter-dataset-collector) or twitter-corpus-
tools (https://github.com/myleott/twitter-corpus-tools) to download the tweets.

## Citing This Dataset 
If you use this dataset in published work, please cite the following paper:

Andrew J. McMinn, Yashar Moshfeghi, Joemon M. Jose. Building a large-scale 
corpus for Evaluating Event Detection on Twitter - Proceedings of the 22nd ACM
international conference on Conference on information & knowledge management.

## Getting Tweet IDs Only
This is useful if you wish to use the twitter-dataset-collector:

    cut -f2 all_ids.tsv > tweet_ids_only.txt


## Getting User IDs Only

    cut -f1 all_ids.tsv > user_ids_only.txt
